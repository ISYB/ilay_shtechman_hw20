﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HW20_IlayShtechman_War_Project
{
    public partial class Form1 : Form
    {
        public static TcpClient client;
        public static NetworkStream clientStream;
        public static IPEndPoint serverEndPoint;
        public static byte[] buffer;
        public static int bytesRead;
        static string card = " ";
        public Form1()
        {
            InitializeComponent();
            //Enabled = false;
            Thread t = new Thread(Game);
            t.Start();
        }

        private void Send()
        {
            string cardStr = Form1.card;
            cardStr = cardStr.Substring(cardStr.LastIndexOf('\\') + 1, cardStr.Length - cardStr.LastIndexOf('\\') - 1);
            char[] sending = new char[4];
            sending[0] = '1';
            int numOfCard = 0;
            if (cardStr.Substring(0, 4) == "king")
            {
                numOfCard = 13;
                sending[1] = '1';
                sending[2] = '3';
            }
            else if (cardStr.Substring(0, 5) == "queen")
            {
                numOfCard = 12;
                sending[1] = '1';
                sending[2] = '2';
            }
            else if (cardStr.Substring(0, 4) == "jack")
            {
                numOfCard = 11;
                sending[1] = '1';
                sending[2] = '1';
            }
            else if (cardStr.Substring(0, 2) == "10")
            {
                numOfCard = 10;
                sending[1] = '1';
                sending[2] = '0';
            }
            else
            {
                numOfCard = int.Parse(cardStr[0].ToString());
                sending[1] = '0';
                sending[2] = numOfCard.ToString()[0];
            }

            sending[3] = 'S';

            buffer = new ASCIIEncoding().GetBytes(sending);
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();



        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.pictureBox2.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox3.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox4.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox5.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox6.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox7.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox8.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox9.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox10.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;

            card = cardPicking();
            Image im = Image.FromFile(card);
            pictureBox1.Image = im;
            Send();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.pictureBox1.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox3.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox4.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox5.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox6.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox7.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox8.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox9.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox10.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;

            card = cardPicking();
            Image im = Image.FromFile(card);
            pictureBox2.Image = im;
            Send();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.pictureBox2.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox1.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox4.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox5.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox6.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox7.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox8.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox9.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox10.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;

            card = cardPicking();
            Image im = Image.FromFile(card);
            pictureBox3.Image = im;
            Send();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            this.pictureBox2.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox3.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox1.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox5.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox6.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox7.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox8.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox9.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox10.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;

            card = cardPicking();
            Image im = Image.FromFile(card);
            pictureBox4.Image = im;
            Send();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.pictureBox2.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox3.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox4.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox1.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox6.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox7.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox8.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox9.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox10.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;

            card = cardPicking();
            Image im = Image.FromFile(card);
            pictureBox5.Image = im;
            Send();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.pictureBox2.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox3.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox4.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox5.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox1.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox7.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox8.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox9.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox10.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;

            card = cardPicking();
            Image im = Image.FromFile(card);
            pictureBox6.Image = im;
            Send();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            this.pictureBox2.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox3.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox4.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox5.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox6.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox1.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox8.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox9.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox10.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;

            card = cardPicking();
            Image im = Image.FromFile(card);
            pictureBox7.Image = im;
            Send();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            this.pictureBox2.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox3.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox4.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox5.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox6.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox7.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox1.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox9.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox10.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;

            card = cardPicking();
            Image im = Image.FromFile(card);
            pictureBox8.Image = im;
            Send();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.pictureBox2.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox3.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox4.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox5.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox6.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox7.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox8.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox1.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox10.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;

            card = cardPicking();
            Image im = Image.FromFile(card);
            pictureBox9.Image = im;
            Send();
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            this.pictureBox2.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox3.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox4.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox5.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox6.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox7.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox8.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox9.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            this.pictureBox1.Image = HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;

            card = cardPicking();
            Image im = Image.FromFile(card);
            pictureBox10.Image = im;
            Send();

        }

        private void quitButton_Click(object sender, EventArgs e)
        {
            Form1.buffer = new ASCIIEncoding().GetBytes("2000");
            Form1.clientStream.Write(buffer, 0, 4);
            Form1.clientStream.Flush();
        }


        public string cardPicking()
        {
            int randNum = 0;
            int randDesc = 0;
            PictureBox tmp = new PictureBox();
            string[] desc = { "_of_clubs.png", "_of_diamonds.png", "_of_spades.png", "_of_hearts.png" };
            string[] nums = { "ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king" };
            Random rnd = new Random();
            randNum = rnd.Next(0, nums.Length);
            randDesc = rnd.Next(0, desc.Length);
            string directory = Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString();
            string img = (directory + @"\Resources\resized\" + (nums[randNum] + desc[randDesc]));
            return img;
        }


        public void Game()
        {
            string input = " ";
            int enemyCardNumber = 0;
            string car = "";
            int ind = 0;
            string card = " ";
            string value = " ";
            int myNum = 0;
            serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client = new TcpClient();
            client.Connect(serverEndPoint);


            while (true)
            {
                //getting information from the server
                clientStream = client.GetStream();
                buffer = new byte[4];
                bytesRead = clientStream.Read(buffer, 0, 4);
                input = new ASCIIEncoding().GetString(buffer);

                if (input == "0000")
                {
                    /*pictureBox1.Enabled = true;
                    pictureBox2.Enabled = true;
                    pictureBox3.Enabled = true;
                    pictureBox4.Enabled = true;
                    pictureBox5.Enabled = true;
                    pictureBox6.Enabled = true;
                    pictureBox7.Enabled = true;
                    pictureBox8.Enabled = true;
                    pictureBox9.Enabled = true;
                    pictureBox10.Enabled = true;*/
                    MessageBox.Show("Connected");
                    GenerateCards();
                }
                else if (input[0] == '1')
                {
                    //Enemy card
                    enemyCardNumber = int.Parse(input.Substring(1, 2));

                    while (Form1.card == " ")
                    {
                        //waiting for the client to pick card
                    }
                    //My card 
                    car = Form1.card;
                    ind = car.LastIndexOf('\\');
                    card = car.Substring(ind + 1, car.Length - ind - 1);
                    value = "";
                    if (card[0] <= '9' && card[0] > '0')
                    {
                        value = card[0] + "";
                    }
                    else if (card.Substring(0, 2) == "10")
                    {
                        value = "10";
                    }
                    else if (card.Substring(0, 3) == "ace")
                    {
                        value = "1";
                    }
                    else if (card.Substring(0, 4) == "king")
                    {
                        value = "13";
                    }
                    else if (card.Substring(0, 5) == "queen")
                    {
                        value = "12";
                    }
                    else if (card.Substring(0, 4) == "jack")
                    {
                        value = "11";
                    }
                    string EnemyCard = GetCardFromData(input.Substring(1, 2), input[3]);    
                    MainPictureBox.Image = Image.FromFile(EnemyCard);
                    myNum = int.Parse(value);
                    if (myNum > enemyCardNumber)
                    {
                        label2.Text = (int.Parse(label2.Text) + 1).ToString();
                        label4.Text = (int.Parse(label2.Text) - 1).ToString();
                        Form1.card = " ";
                        input = " ";
                    }
                    else if (myNum < enemyCardNumber)
                    {
                        label2.Text = (int.Parse(label2.Text) - 1).ToString();
                        label4.Text = (int.Parse(label2.Text) + 1).ToString();
                        Form1.card = " ";
                        input = " ";
                    }
                    
                }
                else if (input[0] == '2')
                {
                    MessageBox.Show("Opponent quited\n" + "Your score : " + label2.Text + "\nOpponent score : " + label4.Text);
                    break;
                }
            }
        }

        private string GetCardFromData(string v1, char v2)
        {
            string pic = "";
            string[] desc = { "_of_clubs.png", "_of_diamonds.png", "_of_spades.png", "_of_hearts.png" };
            string[] nums = { "ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king" };
            if(v1 == "1")
            {
                pic = nums[0];
            }
            else if(v1 == "11")
            {
                pic = nums[10];
            }
            else if(v1 == "12")
            {
                pic = nums[11];
            }
            else if (v1 == "13")
            {
                pic = nums[12];
            }
            else
            {
                pic = nums[int.Parse(v1[1].ToString())];
            }
            
            if (v2 == 'D')
            {
                pic += desc[1];
            }
            else if (v2 == 'S')
            {
                pic += desc[2];
            }
            else if (v2 == 'H')
            {
                pic += desc[3];
            }
            else if (v2 == 'C')
            {
                pic += desc[0];
            }
            string directory = Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString();
            string img = (directory + @"\Resources\resized"+ "\\" + pic);
            return img;
        }

        private void GenerateCards()
        {
            PictureBox blue = new PictureBox();
            blue.Image = global::HW20_IlayShtechman_War_Project.Properties.Resources.card_back_blue;
            blue.Size = new Size(90, 114);

            PictureBox red = new PictureBox();
            red.Image = global::HW20_IlayShtechman_War_Project.Properties.Resources.card_back_red;
            red.Size = new Size(90, 114);

            pictureBox1.Image = red.Image;
            pictureBox2.Image = red.Image;
            pictureBox3.Image = red.Image;
            pictureBox4.Image = red.Image;
            pictureBox5.Image = red.Image;
            pictureBox6.Image = red.Image;
            pictureBox7.Image = red.Image;
            pictureBox8.Image = red.Image;
            pictureBox9.Image = red.Image;
            pictureBox10.Image = red.Image;
            pictureBox1.Image = red.Image;
            pictureBox1.Image = red.Image;
            MainPictureBox.Image = blue.Image;
        }

    }
}
